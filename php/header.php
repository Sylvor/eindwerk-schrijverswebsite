<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$gebruikersnaam = "gebruikersnaam";
$gevraagdeUrl = $_SERVER['REQUEST_URI'];
$explodedGevraagdeUrl = explode("/", $gevraagdeUrl);
$domeinNaam = $explodedGevraagdeUrl[1];

if (isset($_SESSION[$gebruikersnaam])) {
    echo "<a id=\"logInOut\" href=\"/$domeinNaam/webpages/uitloggenWebpage.php\">Uitloggen</a>";
} else {
    echo "<a id=\"logInOut\" href=\"/$domeinNaam/webpages/inloggenWebpage.php\">Inloggen</a><br>";
    echo "<a id=\"logInOut\" href=\"/$domeinNaam/webpages/registrerenWebpage.php\">Registreren</a>";
}