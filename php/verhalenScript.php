<?php

include 'databankConnectie.php';

$gevraagdeUrl = $_SERVER['REQUEST_URI'];
$explodedGevraagdeUrl = explode("/", $gevraagdeUrl);
$domeinNaam = $explodedGevraagdeUrl[1];
$gebruikerstabel = "gebruiker";
$verhalentabel = "verhaal";
$verhalenCommentaartabel = "verhaalcomment";

function toonVerhalen(/*$var1, $var2, $var3 = 'somevalue'*/) {
    $zoekTerm = "";
    if (isset($_POST['verhalenZoeken'])) {
        $zoekTerm = $_POST['zoekveld'];
    }
    $connection = openConnection();
    $sql = "SELECT v.id, v.titel, v.auteurId, v.rating, v.korteBeschrijving, v.inhoudVerhaal, v.creatiedatum, g.nickname FROM "
            . $GLOBALS['gebruikerstabel'] . " g JOIN " . $GLOBALS['verhalentabel'] . " v on v.auteurId = g.id WHERE g.nickname like '%"
            . $zoekTerm . "%' or v.titel like '%" . $zoekTerm . "%'";
    $result = $connection->query($sql);
    echo "<form id=\"verhalenFilter\" action=\"\" method=\"post\"><input type=\"text\" name=\"zoekveld\" placeholder=\"Zoek op naam of auteur\">"
    . "<input type=\"submit\" name=\"verhalenZoeken\" value=\"Zoek\"></form>";
    if ($result->num_rows > 0) {
        echo "<table border='1'>
<tr>
<th>Titel</th>
<th>Auteur</th>
<th>Rating</th>
<th>Korte beschrijving</th>
<th>Datum van creatie</th>
</tr>";

        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td><a href=\"/" . $GLOBALS['domeinNaam'] . "/webpages/verhaalDetail.php?id=" . $row ["id"] . "\">" . $row["titel"] . "</a></td>";
            echo "<td>" . $row["nickname"] . "</td>";
            echo "<td>" . $row["rating"] . "</td>";
            echo "<td>" . $row["korteBeschrijving"] . "</td>";
            echo "<td>" . $row["creatiedatum"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "0 results";
    }
    closeConnection($connection);
}

function verhaalDetail($id) {
    $connection = openConnection();
    $gebruikersnaam = "gebruikersnaam";
    $sql = "SELECT v.auteurId, v.rating, v.korteBeschrijving, v.titel, v.inhoudVerhaal, v.creatiedatum, g.nickname FROM "
            . $GLOBALS['gebruikerstabel'] . " g JOIN " . $GLOBALS['verhalentabel'] . " v on v.auteurId = g.id WHERE v.id = " . $id;
    $result = $connection->query($sql);

    if ($result->num_rows > 0) {
        echo "<table border='1'>
<tr>
<th>Titel</th>
<th>Auteur</th>
<th>Rating</th>
<th>Korte beschrijving</th>
<th>Inhoud verhaal</th>
<th>Datum van creatie</th>
</tr>";

        $IsEigenaar = false;
        echo "<form id=\"Opslaan\" action=\"\" method=\"post\">";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            $IsEigenaar = isset($_SESSION[$gebruikersnaam]) && $row["nickname"] == $_SESSION[$gebruikersnaam];
            if ($IsEigenaar) {
                echo "<td><textarea name=\"titelEditor\" rows=\"10\" cols=\"50\">" . $row["titel"] . "</textarea></td>";
                echo "<td>" . $row["nickname"] . "</td>";
                echo "<td>" . $row["rating"] . "</td>";
                echo "<td><textarea name=\"beschrijvingEditor\" rows=\"10\" cols=\"50\">" . $row["korteBeschrijving"] . "</textarea></td>";
                echo "<td><textarea name=\"verhaalEditor\" rows=\"10\" cols=\"100\">" . $row["inhoudVerhaal"] . "</textarea></td>";
            } else {
                echo "<td>" . $row["titel"] . "</td>";
                echo "<td>" . $row["nickname"] . "</td>";
                echo "<td>" . $row["rating"] . "</td>";
                echo "<td>" . $row["korteBeschrijving"] . "</td>";
                echo "<td><textarea readonly=\"readonly\" rows=\"10\" cols=\"100\">" . $row["inhoudVerhaal"] . "</textarea></td>";
            }
            echo "<td>" . $row["creatiedatum"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        if ($IsEigenaar) {
            echo "<input type=\"hidden\" name=\"id\" value=\"" . $id . "\"/><input type=\"submit\" name=\"VerhaalSave\" value=\"Opslaan\">";
        }
        echo "</form>";
    } else {
        echo "0 results";
    }
    closeConnection($connection);
}

function editVerhaal($id, $verhaal, $beschrijving, $titel) {
    $connection = openConnection();
    $sql = "UPDATE " . $GLOBALS['verhalentabel'] . " SET inhoudVerhaal = '" . $verhaal . "', korteBeschrijving = '" . $beschrijving . "', titel = '" 
            . $titel . "' WHERE id  = '" . $id . "'";
    $result = $connection->query($sql);
    closeConnection($connection);
}

function verhaalToevoegen($titel, $beschrijving, $verhaal) {
    $connection = openConnection();
    $rating = 0;
    $auteurId = $_SESSION['gebruikersId'];
    $datum = date("Y-m-d H:i:s", time());
    $sql = "SELECT v.auteurId, v.rating, v.korteBeschrijving, v.titel, v.inhoudVerhaal, v.creatiedatum, g.nickname FROM "
            . $GLOBALS['gebruikerstabel'] . " g JOIN " . $GLOBALS['verhalentabel'] . " v on v.auteurId = g.id WHERE v.titel = '" . $titel 
            . "' and v.auteurId = " . $auteurId . "";
    $result = $connection->query($sql);

    if ($result->num_rows != 0) {
        closeConnection($connection);
        echo "<script type='text/javascript'>alert('Verhaal met zelfde titel voor gebruiker gevonden')</script>";
        return false;
    } else {
        $sql = "INSERT INTO " . $GLOBALS['verhalentabel'] . " (auteurId, titel, rating, "
                . "korteBeschrijving, inhoudVerhaal, creatiedatum) VALUES ('" . $auteurId . "', '" . $titel . "', "
                . "'" . $rating . "', '" . $beschrijving . "', '" . $verhaal . "', '" . $datum . "')";
        if ($connection->query($sql) === TRUE) {
            closeConnection($connection);
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . $connection->error;
            closeConnection($connection);
        }
    }
}

function showCommentaar($verhaalId) {
    $connection = openConnection();
    $sql = "SELECT vc.id as commentid, vc.verhaalId, vc.commentaar, vc.fromGebruikerId, vc.commentaarDatum, g.id, g.nickname FROM "
            . $GLOBALS['verhalenCommentaartabel'] . " vc JOIN " . $GLOBALS['gebruikerstabel'] . " g on vc.fromGebruikerId = g.id and vc.verhaalId = " . $verhaalId;
    $result = $connection->query($sql);
    if ($result->num_rows > 0) {
        echo "<table border='1'>
<tr>
<th>Naam </th>
<th>Post </th>
<th>Datum: </th>
</tr>";

        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td><a href=\"/" . $GLOBALS['domeinNaam'] . "/webpages/gebruikerspagina.php?id=" . $row ["fromGebruikerId"] . "\">" . $row["nickname"] . "</a></td>";
            echo "<td>" . $row["commentaar"] . "</td>";
            echo "<td>" . $row["commentaarDatum"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "Geen commentaar aanwezig";
    }
    closeConnection($connection);
}

function plaatsCommentaar($verhaalId, $fromGebruikerId, $commentaar) {
    $connection = openConnection();
    $datum = date("Y-m-d H:i:s", time());
    $sql = "INSERT INTO " . $GLOBALS['verhalenCommentaartabel']
            . " (verhaalId, fromGebruikerId, commentaar, commentaarDatum) VALUES ('"
            . $verhaalId . "', '" . $fromGebruikerId . "', '" . $commentaar . "', '" . $datum . "')";
    $result = $connection->query($sql);
    closeConnection($connection);
}