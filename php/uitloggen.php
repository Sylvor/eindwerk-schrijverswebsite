<?php

function uitloggen() {
    session_start();
    session_unset();
    session_destroy();
    header("Location: ../homepage.php"); /* Redirect browser */
    exit();
}

?>