<?php

const TABEL_GEBRUIKER ="gebruiker";
$servername = "localhost";
$username = "root";
$dbname = "wegwijzer";
$password = "admin";

function openConnection() {
    $connection = new mysqli($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']) OR DIE("Unable to 
connect to database! Please try again later.");
    $connection->select_db($GLOBALS['dbname']);
    return $connection;
}

function closeConnection($connection) {
    mysqli_close($connection);
}
