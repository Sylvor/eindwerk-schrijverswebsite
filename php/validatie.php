<?php

$naamError = $emailError = $wachtwoordError = "";
$naam = $email = $wachtwoord = "";

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["gebruikersnaam"])) {
        $naamError = "Naam is niet ingevuld";
    } else {
        $naam = test_input($_POST["gebruikersnaam"]);
        if (!preg_match("/^[a-zA-Z_]*$/", $naam)) {
            $naamError = "Enkel letters en underscores toegestaan";
        }
    }

    if (empty($_POST["wachtwoord"])) {
        $wachtwoordError = "Wachtwoord is niet ingevuld";
    } else {
        $wachtwoord = test_input($_POST["wachtwoord"]);
        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,15}$/', $wachtwoord)) {
            $wachtwoordError = "Het wachtwoord is niet geldig";
        }
    }

    if (empty($_POST["email"])) {
        $emailError = "E-mailadres is niet ingevuld";
    } else {
        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailError = "Gelieve een geldig e-mailadres te gebruiken";
        }
    }
}
?>