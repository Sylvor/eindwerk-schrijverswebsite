<?php

$gebruikersnaam = "gebruikersnaam";
$gevraagdeUrl = $_SERVER['REQUEST_URI'];
$explodedGevraagdeUrl = explode("/", $gevraagdeUrl);
$domeinNaam = $explodedGevraagdeUrl[1];

echo "<ul><li>
                <a href=\"/$domeinNaam/homepage.php\">Homepagina</a>
            </li>";
if (isset($_SESSION[$gebruikersnaam])) {
    echo "<li><a href=\"/$domeinNaam/webpages/gebruikerspagina.php\">Mijn gebruikerspagina</a></li>";
}
echo "
<li>
    <a href=\"/$domeinNaam/webpages/verhalen.php\">Verhalen</a>
</li>
<li>
    <a href=\"/$domeinNaam/webpages/gebruikers.php\">Gebruikers</a>
</li>
<li>
    <a href=\"/$domeinNaam/webpages/forum.php\">Forum</a>
</li></ul>";
