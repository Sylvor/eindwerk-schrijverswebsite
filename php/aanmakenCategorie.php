<?php

include 'databankConnectie.php';

$connection = openConnection();

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "<form method='post' action=''>
        Categorienaam: <input type='text' name='naam' />
        Beschrijving: <textarea name='beschrijving' /></textarea>
        <input type='submit' value='Add category' />
     </form>";
} else {
    $sql = "INSERT INTO categorie(naam, beschrijving) 
        VALUES('" . $_POST['naam'] . "',
        '" . $_POST['beschrijving'] . "')";
    $result = $connection->query($sql);
    if (!$result) {
        echo "Error: " . $sql . "<br>" . $connection->error;
        closeConnection($connection);
    } else {
        echo 'Nieuwe categorie toegevoegd.';
        closeConnection($connection);
    }
}