<?php

include 'databankConnectie.php';

$gebruikerstabel = "gebruiker";
$gebruikersnaam = "gebruikersnaam";
$verhalentabel = "verhaal";
$gebruikersCommentaartabel = "gebruikerscomment";
$vriendentabel = "vriendverzoek";

function vraagGebruikersCreatieDatum($gebruikersnaam) {
    $connection = openConnection();
    $query = "SELECT creatiedatum FROM " . TABEL_GEBRUIKER . " WHERE nickname  = '" . $gebruikersnaam . "'";
    $result = $connection->query($query);
    if ($result->num_rows == 1) {
        $row = mysqli_fetch_array($result);
        $beschrijving = $row["creatiedatum"];
        closeConnection($connection);
        return $beschrijving;
    } else {
        closeConnection($connection);
        return "";
    }
}

function vraagGebruikersBeschrijving($gebruikersnaam) {
    $connection = openConnection();
    $query = "SELECT beschrijving FROM " . $GLOBALS['gebruikerstabel'] . " WHERE nickname  = '" . $gebruikersnaam . "'";
    $result = $connection->query($query);
    if ($result->num_rows == 1) {
        $row = mysqli_fetch_array($result);
        $beschrijving = $row["beschrijving"];
        closeConnection($connection);
        return $beschrijving;
    } else {
        closeConnection($connection);
        return "";
    }
}

function editGebruikersBeschrijving($gebruikersnaam, $beschrijving) {
    $connection = openConnection();
    $sql = "UPDATE " . $GLOBALS['gebruikerstabel'] . " SET beschrijving = '" . $beschrijving . "' WHERE nickname  = '" . $gebruikersnaam . "'";
    $result = $connection->query($sql);
    closeConnection($connection);
}

function isIngelogdeGebruiker() {
    parse_str($_SERVER['QUERY_STRING']);
    if (isset($_SESSION['gebruikersId'])) {
        if (isset($id)) {
            return $_SESSION['gebruikersId'] === $id;
        }
        return true;
    } else {
        return false;
    }
}

function gebruikerDetails($id) {
    if (isIngelogdeGebruiker()) {
        echo "Gebruikersnaam: " . $_SESSION['gebruikersnaam'] . "<br>";
        echo "Lid sinds: " . vraagGebruikersCreatieDatum($_SESSION['gebruikersnaam']) . "<br>";
        echo "Beschrijving: " . vraagGebruikersBeschrijving($_SESSION['gebruikersnaam']) . "<br>";
        echo "<form id=\"Opslaan\" action=\"\" method=\"post\">
                <textarea name=\"beschrijvingsEditor\" placeholder=\"Pas hier uw beschrijving aan.\" rows=\"4\" cols=\"50\"></textarea><br>
                <input type=\"submit\" name=\"beschrijvingsEditorSave\" value=\"Opslaan\">
            </form>";
    } else {
        $connection = openConnection();
        $sql = "SELECT id, nickname, email, leeftijd, beschrijving, creatiedatum FROM "
                . $GLOBALS['gebruikerstabel'] . " WHERE id = " . $id;
        $result = $connection->query($sql);
        if ($result->num_rows > 0 && $row = mysqli_fetch_array($result)) {
            echo "Gebruikersnaam: " . $row["nickname"] . "<br>";
            echo "Lid sinds: " . $row["creatiedatum"] . "<br>";
            echo "Beschrijving: " . $row["beschrijving"] . "<br>";
            closeConnection($connection);
        } else {
            closeConnection($connection);
            echo "No results";
        }
    }
}

function toonGebruikers() {
    $zoekTerm = "";
    if (isset($_POST['gebruikersZoeken'])) {
        $zoekTerm = $_POST['zoekveld'];
    }
    $connection = openConnection();
    $sql = "SELECT id, nickname, email, leeftijd, beschrijving, creatiedatum FROM "
            . $GLOBALS['gebruikerstabel']
            . " WHERE nickname like '%"
            . $zoekTerm . "%'";
    $result = $connection->query($sql);
    echo "<form id=\"gebruikersFilter\" action=\"\" method=\"post\"><input type=\"text\" name=\"zoekveld\" placeholder=\"Zoek op naam\">"
    . "<input type=\"submit\" name=\"gebruikersZoeken\" value=\"Zoek\"></form>";
    if ($result->num_rows > 0) {
        echo "<table border='1'>
    <tr>
        <th>Id</th>
        <th>Gebruikersnaam</th>
        <th>E-mailadres</th>
        <th>Leeftijd</th>
        <th>Beschrijving</th>
        <th>Creatiedatum</th>
    </tr>";

        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row["id"] . "</td>";
            echo "<td><a href=\"/" . $GLOBALS['domeinNaam'] . "/webpages/gebruikerspagina.php?id=" . $row ["id"] . "\">" . $row["nickname"] . "</a></td>";
            echo "<td>" . $row["email"] . "</td>";
            echo "<td>" . $row["leeftijd"] . "</td>";
            echo "<td>" . $row["beschrijving"] . "</td>";
            echo "<td>" . $row["creatiedatum"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "0 results";
    }
    closeConnection($connection);
}

function showCommentaar($accountId) {
    $connection = openConnection();
    $sql = "SELECT gc.id as commentid, gc.accountId, gc.fromGebruikerId, gc.commentaar, gc.commentaarDatum, g.nickname FROM "
            . $GLOBALS['gebruikersCommentaartabel'] . " gc JOIN " . $GLOBALS['gebruikerstabel'] . " g on gc.fromGebruikerId = g.id and gc.accountId = " . $accountId;
    $result = $connection->query($sql);
    if ($result->num_rows > 0) {
        echo "<table border='1'>
<tr>
<th>Naam </th>
<th>Post </th>
<th>Datum </th>
</tr>";

        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td><a href=\"/" . $GLOBALS['domeinNaam'] . "/webpages/gebruikerspagina.php?id=" . $row ["fromGebruikerId"] . "\">" . $row["nickname"] . "</a></td>";
            echo "<td>" . $row["commentaar"] . "</td>";
            echo "<td>" . $row["commentaarDatum"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "Geen commentaar aanwezig";
    }
    closeConnection($connection);
}

function plaatsCommentaar($accountId, $fromGebruikerId, $commentaar) {
    $connection = openConnection();
    $datum = date("Y-m-d H:i:s", time());
    $sql = "INSERT INTO " . $GLOBALS['gebruikersCommentaartabel']
            . " (accountId, fromGebruikerId, commentaar, commentaarDatum) VALUES ('"
            . $accountId . "', '" . $fromGebruikerId . "', '" . $commentaar . "', '" . $datum . "')";
    $result = $connection->query($sql);
    closeConnection($connection);
}

function sendMail($emailadres, $nickGeadresseerde, $nickZender) {
    require 'PHPMailerAutoload.php';

    $mail = new PHPMailer;
    $mail->setFrom('bot@wegwijzer.be', 'De Wegwijzer');
    $mail->addAddress($emailadres, $nickGeadresseerde);
    /*$mail->addReplyTo("xxxx@domainname.com", "Reply");*/
    $mail->isHTML(true);

    $mail->Subject = "Vriendaanvraag";
    $mail->Body = "<i>$nickZender heeft u een vriendaanvraag verstuurd, $nickGeadresseerde</i>";
    $mail->AltBody = "This is the plain text version of the email content";

    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message has been sent successfully";
    }
}

function vriendVerzoekVersturen($idVerzender, $idGeadreseerde) {
    $connection = openConnection();
    $sqlVrienden = "SELECT id  FROM "
            . $GLOBALS['vriendentabel'] . " where gebruiker1Id in ($idVerzender, $idGeadreseerde) and gebruiker2Id in ($idVerzender, $idGeadreseerde)";
    $resultVrienden = $connection->query($sqlVrienden);
    if ($resultVrienden->num_rows > 0) {
        closeConnection($connection);
        return false;
    }

    $sqlVerzender = "SELECT id, nickname  FROM "
            . $GLOBALS['gebruikerstabel'] . " where id = $idVerzender";
    $resultVerzender = $connection->query($sqlVerzender);
    if ($resultVerzender->num_rows != 1) {
        closeConnection($connection);
        return false;
    } else {
        if ($row = mysqli_fetch_array($resultVerzender)) {
            $verzenderNickname = $row["nickname"];
        }
    }

    $sqlGeadreseerde = "SELECT id, nickname, email  FROM "
            . $GLOBALS['gebruikerstabel'] . " where id = $idGeadreseerde";
    $resultGeadreseerde = $connection->query($sqlGeadreseerde);
    if ($resultGeadreseerde->num_rows != 1) {
        closeConnection($connection);
        return false;
    } else {
        if ($row = mysqli_fetch_array($resultGeadreseerde)) {
            $geadreseerdeNickname = $row["nickname"];
            $geadreseerdeEmail = $row["email"];
        }
    }

    if (isset($verzenderNickname) && isset($geadreseerdeNickname) && isset($geadreseerdeEmail)) {
        $msg = $verzenderNickname . " heeft je een vriendschapsverzoek gestuurd, " . $geadreseerdeNickname;
        $insertSql = "INSERT INTO vriendverzoek (gebruiker1Id, gebruiker2Id) VALUES ($idVerzender, $idGeadreseerde)";
        print_r($insertSql);
        if ($connection->query($insertSql) === TRUE) {
            closeConnection($connection);
            return true;
        } else {
            echo "Error: " . $insertSql . "<br>" . $connection->error;
            closeConnection($connection);
        }
        mail($geadreseerdeEmail, "vriendschapsverzoek", $msg);
        print_r(error_get_last());
        closeConnection($connection);
        return true;
    }
    return false;
}
