CREATE DATABASE IF NOT EXISTS wegwijzer;

USE wegwijzer;

CREATE TABLE IF NOT EXISTS gebruiker(
id int NOT NULL auto_increment,
nickname varchar(20) NOT NULL,
email varchar(40) NOT NULL,
wachtwoord varchar(14) NOT NULL,
isAdmin bool,
leeftijd int,
beschrijving text,
creatiedatum datetime NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS verhaal(
id int NOT NULL auto_increment,
auteurId int NOT NULL,
titel text NOT NULL,
rating int,
korteBeschrijving text NOT NULL,
inhoudVerhaal text NOT NULL,
creatiedatum datetime NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (auteurId) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS tag(
tagId int NOT NULL auto_increment,
tag varchar(30) NOT NULL,
PRIMARY KEY (tagId)
);

CREATE TABLE IF NOT EXISTS gebruikerrating(
id int NOT NULL auto_increment,
fromGebruikerId int NOT NULL,
rating int NOT NULL,
toGebruikerId int NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (fromGebruikerId) REFERENCES gebruiker(id),
FOREIGN KEY (toGebruikerId) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS vriendverzoek(
id int NOT NULL auto_increment,
gebruiker1Id int NOT NULL,
gebruiker2Id int NOT NULL,
isGoedgekeurd boolean default false,
PRIMARY KEY (id),
FOREIGN KEY (gebruiker1Id) REFERENCES gebruiker(id),
FOREIGN KEY (gebruiker2Id) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS message(
berichtId int NOT NULL auto_increment,
fromGebruikerId int NOT NULL,
bericht text NOT NULL,
toGebruikerId int NOT NULL,
berichtDatum datetime NOT NULL,
PRIMARY KEY (berichtId),
FOREIGN KEY (fromGebruikerId) REFERENCES gebruiker(id),
FOREIGN KEY (toGebruikerId) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS verhaalrating(
id int NOT NULL auto_increment,
verhaalId int NOT NULL,
gebruikerId int NOT NULL,
rating int NOT NULL,
ratingDatum datetime NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (gebruikerId) REFERENCES gebruiker(id),
FOREIGN KEY (verhaalId) REFERENCES verhaal(id)
);

CREATE TABLE IF NOT EXISTS verhaalcomment(
id int NOT NULL auto_increment,
verhaalId int NOT NULL,
commentaar text NOT NULL,
fromGebruikerId int NOT NULL,
commentaarDatum datetime NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (fromGebruikerId) REFERENCES gebruiker(id),
FOREIGN KEY (verhaalId) REFERENCES verhaal(id)
);

CREATE TABLE IF NOT EXISTS verhaaltag(
id int NOT NULL auto_increment,
verhaalId int NOT NULL,
tagId int NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (verhaalId) REFERENCES verhaal(id),
FOREIGN KEY (tagId) REFERENCES tag(tagId)
);

CREATE TABLE IF NOT EXISTS gebruikerscomment(
id int NOT NULL auto_increment,
accountId int NOT NULL,
commentaar text NOT NULL,
fromGebruikerId int NOT NULL,
commentaarDatum datetime NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (fromGebruikerId) REFERENCES gebruiker(id),
FOREIGN KEY (accountId) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS categorie(
id int NOT NULL auto_increment,
naam varchar(30) NOT NULL,
beschrijving varchar(250),
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS topic(
id int NOT NULL auto_increment,
onderwerp varchar(100) NOT NULL,
datum datetime NOT NULL,
categorie int NOT NULL,
gebruiker int NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (categorie) REFERENCES categorie(id),
FOREIGN KEY (gebruiker) REFERENCES gebruiker(id)
);

CREATE TABLE IF NOT EXISTS post(
id int NOT NULL auto_increment,
inhoud text NOT NULL,
datum datetime NOT NULL,
topic int NOT NULL,
gebruiker int NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (topic) REFERENCES topic(id),
FOREIGN KEY (gebruiker) REFERENCES gebruiker(id)
);