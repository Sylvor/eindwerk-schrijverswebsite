<html>
    <head>
        <meta charset="UTF-8">
        <title></title>        
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <h1>Forum</h1>
            <a class="item" href="../php/aanmakenTopic.php">Maak een topic aan</a> -
            <a class="item" href="../php/aanmakenCategorie.php">Maak een categorie aan</a>
            <div id="content">
                <?php
                include '../php/databankConnectie.php';

                $connection = openConnection();

                $sql = "SELECT id, naam, beschrijving FROM categorie";

                $result = $connection->query($sql);

                if (!$result) {
                    echo 'De categorieën kunnen niet worden weergegeven.';
                } else {
                    if (mysql_num_rows($result) > 0) {
                        echo "<table border=\"1\">
                    <tr>
                        <th>Categorie</th>
                        <th>Laatste topic</th>
                    </tr>";

                        while ($row = mysqli_fetch_array($result)) {
                            echo "<tr>";
                            echo "<td>" . $row["naam"] . "</td>";
                            echo "<td>" . $row["beschrijving"] . "</td>";
                            echo "</tr>";
                        }
                        closeConnection($connection);
                    } else {
                        echo 'Er zijn nog geen categorieën aangemaakt.';
                        closeConnection($connection);
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>
