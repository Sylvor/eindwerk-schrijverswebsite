<?php
include '../php/registreren.php';
include '../php/validatie.php';
if ($_POST) {
    if (empty($naamError) && empty($wachtwoordError) && empty($emailError)) {
        $geregistreerd = registreren($_POST['gebruikersnaam'], $_POST['wachtwoord'], $_POST['email']);
        if ($geregistreerd) {
            header("Location: ../homepage.php");
        } else {
            echo "<script type='text/javascript'>alert('Niet geregistreerd, gebruiker of email bestaat al')</script>";
        }
    }
}
?> 

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <form id="registreren" action="" method="post">
                REGISTREREN <br>
                Gebruikersnaam: <input type="text" name="gebruikersnaam" value="<?php echo $naam; ?>"> <span class="error">* <?php echo $naamError; ?></span> <br>
                Wachtwoord: <input type="password" name="wachtwoord" value="<?php echo $wachtwoord; ?>"> <span class="error">* <?php echo $wachtwoordError; ?></span> <br>
                <p>Het wachtwoord moet minstens 6 tekens en maximaal 15 tekens lang zijn. <br>
                    Het moet minstens 1 letter en 1 cijfer bevatten. <br>
                    De tekens mogen zowel letters, nummers als een van volgende
                    speciale karakters zijn: !@#$%<br></p>
                E-mail: <input type="email" name="email" value="<?php echo $email; ?>"> <span class="error">* <?php echo $emailError; ?></span> <br>
                <input type="submit">
            </form>
        </div>
    </body>
</html>
