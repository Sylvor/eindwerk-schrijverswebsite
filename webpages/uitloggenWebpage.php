<?php
include '../php/uitloggen.php';
if (isset($_POST['uitlogknop'])) {
    uitloggen();
    echo "<script type='text/javascript'>alert('Uitgelogd!')</script>";
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>  
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <form id="uitloggen" action="" method="post">
                <input type="submit" name="uitlogknop" value="UITLOGGEN">
            </form>
        </div>
    </body>
</html>
