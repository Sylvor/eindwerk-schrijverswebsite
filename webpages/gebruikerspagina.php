<?php
include '../php/gebruikersScript.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['beschrijvingsEditorSave'])) {
    editGebruikersBeschrijving($_SESSION['gebruikersnaam'], $_POST['beschrijvingsEditor']);
}

if (isset($_POST['postCommentGebruiker'])) {
    plaatsCommentaar($_POST['idGeadreseerde'], $_SESSION['gebruikersId'], $_POST['commentGebruiker']);
}

if (isset($_POST['vriendAanvraag'])) {
    vriendVerzoekVersturen($_POST['idZender'], $_POST['idGeadreseerde']);
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <?php
            $gebruiker;
            parse_str($_SERVER['QUERY_STRING']);
            if (isset($id)) {
                $gebruiker = $id;
            } elseif (isset($_SESSION['gebruikersId'])) {
                $gebruiker = $_SESSION['gebruikersId'];
            }
            if (isset($gebruiker)) {
                gebruikerDetails($gebruiker);
                ?>
                <div id="comments">
                    <?php
                    showCommentaar($gebruiker);
                    ?>
                </div>
                <form id="postCommentGebruiker" action="" method="post">
                    <textarea name="commentGebruiker" placeholder="Schrijf hier uw commentaar" rows="5" cols="60"></textarea>
                    <input type="submit" name="postCommentGebruiker" value="Post comment"/>
                    <input type="hidden" name="idGeadreseerde" value="<?php echo $gebruiker ?>"/>
                </form>
                <form id="vriendAanvraagPost" action="" method="post">
                    <input type="submit" name="vriendAanvraag" value="Vriendverzoek versturen"/>
                    <input type="hidden" name="idZender" value="<?php echo $_SESSION['gebruikersId'] ?>"/>
                    <input type="hidden" name="idGeadreseerde" value="<?php echo $gebruiker ?>"/>
                </form> 
                <?php
            } else {
                header("Location: gebruikers.php"); /* Redirect browser */
            }
            ?> 

        </div>
        <footer>

        </footer>
    </body>
</html>
