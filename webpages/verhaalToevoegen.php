<?php
include '../php/verhalenScript.php';
if ($_POST) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $verhaalToegevoegd = verhaalToevoegen($_POST['titel'], $_POST['verhaalBeschrijvingToevoegen'], $_POST['verhaalInhoudToevoegen']);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <form id="VerhaalToevoegen" action="" method="post">
                Titel: <input type="text" name="titel"><br>                
                <textarea name="verhaalBeschrijvingToevoegen" placeholder="Vul hier uw beschrijving in." rows="4" cols="50"></textarea><br>
                <textarea name="verhaalInhoudToevoegen" placeholder="Vul hier uw verhaal in." rows="4" cols="50"></textarea><br>
                <input type="submit" name="verhaalToevoegenSave" value="Verhaal toevoegen">
            </form>
        </div>
        <footer>
        </footer>
    </body>
</html>