<?php
include '../php/verhalenScript.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_POST['VerhaalSave'])) {

    editVerhaal($_POST['id'], $_POST['verhaalEditor'], $_POST['beschrijvingEditor'], $_POST['titelEditor']);
}
if (isset($_POST['postCommentVerhaal'])) {
    plaatsCommentaar($_POST['idVerhaal'], $_SESSION['gebruikersId'], $_POST['commentVerhaal']);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <?php
            parse_str($_SERVER['QUERY_STRING']);
            if (isset($id)) {
                verhaalDetail($id);
                ?>
                <div id="ratingVerhaal">

                </div>
                <form id="postRatingVerhaal" action="" method="post">
                    <select>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>                    
                        <option value="5">5</option>
                    </select>
                </form>
                <div id="comments">
                    <?php
                    showCommentaar($id);
                    ?>
                </div>
                <form id="postCommentVerhaal" action="" method="post">
                    <textarea name="commentVerhaal" placeholder="Schrijf hier uw commentaar" rows="5" cols="60"></textarea>
                    <input type="submit" name="postCommentVerhaal" value="Post comment"/>
                    <input type="hidden" name="idVerhaal" value="<?php echo $id ?>"/>
                </form>
                <?php
            } else {
                header("Location: verhalen.php"); /* Redirect browser */
            }
            ?>

        </div>
        <footer>

        </footer>
    </body>
</html>
