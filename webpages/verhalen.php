<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <?php
            include '../php/verhalenScript.php';
            echo "<a href=\"/" . $GLOBALS['domeinNaam'] . "/webpages/verhaalToevoegen.php\">Verhaal toevoegen</a>";
            toonVerhalen();
            ?>
        </div>
    </body>
</html>
