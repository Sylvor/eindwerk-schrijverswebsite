<?php
include '../php/authenticatie.php';
if ($_POST) {
    $authenticated = authenticate($_POST['gebruikersnaam'], $_POST['wachtwoord']);
    if ($authenticated) {
        header("Location: ../homepage.php");
    } else {
        echo "<script type='text/javascript'>alert('Niet ingelogd!')</script>";
    }
}
?>


<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <header>
            <?php include '../php/header.php'; ?>
        </header>
        <nav>
            <?php include '../php/nav.php'; ?>
        </nav>
        <div class="feed">
            <?php
            echo "<b>PLACEHOLDER FEED</b>"
            ?>
        </div>
        <div class="main">
            <form id="inloggen" action="" method="post">
                INLOGGEN
                Gebruikersnaam: <input type="text" name="gebruikersnaam">
                Wachtwoord: <input type="password" name="wachtwoord">
                <input type="submit">
            </form>
        </div>
    </body>
</html>
